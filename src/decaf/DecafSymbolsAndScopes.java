package decaf;
import org.antlr.symtab.FunctionSymbol;
import org.antlr.symtab.GlobalScope;
import org.antlr.symtab.LocalScope;
import org.antlr.symtab.Scope;
import org.antlr.symtab.VariableSymbol;
import org.antlr.symtab.Symbol;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

/**
 * This class defines basic symbols and scopes for Decaf language
 */
 
public class DecafSymbolsAndScopes extends DecafParserBaseListener {
	ParseTreeProperty<Scope> scopes = new ParseTreeProperty<Scope>();
	GlobalScope globals;
	Scope currentScope; // define symbols in this scope
	int var_metodo = 0;
	int var_chamada_metodo = 0;

	@Override
	public void enterProgram(DecafParser.ProgramContext ctx) {
		globals = new GlobalScope(null);
		pushScope(globals);
   	}

    @Override
    public void exitProgram(DecafParser.ProgramContext ctx) {
       	//System.out.println(globals);
		Symbol var = currentScope.resolve("main");
		for(int i = 0; i < ctx.metodo().size(); i++){
			if ( var==null ) {
            	this.error(ctx.metodo(i).ID().getSymbol(), "Método main não declarado");
			}
		}
		popScope();
    }

    @Override
    public void enterMetodo(DecafParser.MetodoContext ctx) {
        String name = ctx.ID().getText();
        //int typeTokenType = ctx.tipo().start.getType();
        //DecafSymbol.Type type = this.getType(typeTokenType);

        // push new scope by making new one that points to enclosing scope
        FunctionSymbol function = new FunctionSymbol(name);
        // function.setType(type); // Set symbol type

        currentScope.define(function); // Define function in current scope
        saveScope(ctx, function);
        pushScope(function);
    }

    @Override
    public void exitMetodo(DecafParser.MetodoContext ctx) {
        //System.out.println("Teste: " + ctx.VOID());
        if(ctx.VOID() != null){
            //System.out.println("Encontrou void");
            //System.out.println("Return: " + ctx.getText());
            for (int i = 0; i < ctx.bloco().conteudo().size(); i++){
                //System.out.println("Return: " + ctx.bloco().conteudo().getText());
                if (ctx.bloco().conteudo(i).RETURN() != null){
                    //System.out.println("Encontrou RETURN");
                    this.error(ctx.bloco().conteudo(i).RETURN().getSymbol(), "Método void não deve retornar nada");
                }
            }
        }
        
        try{
            if(ctx.tipo().INT() != null){
                if(ctx.bloco().conteudo(0).RETURN() != null){
                    if(ctx.bloco().conteudo(0).expressao(0).literal().BL() != null){
                        this.error(ctx.bloco().conteudo(0).expressao(0).literal().BL().getSymbol(), "Método int deve retornar inteiro");
                    }
                }
            }
        } catch(Exception ex){}            

        popScope();
    }

    @Override
    public void enterBloco(DecafParser.BlocoContext ctx) {
    	/*LocalScope l = new LocalScope(currentScope);
    	//System.out.println("Escopo: "+ currentScope);
    	saveScope(ctx, currentScope);
    	pushScope(l);*/
    }

    @Override
    public void exitBloco(DecafParser.BlocoContext ctx) {
    	// popScope();
    }

    @Override
    public void enterDecl(DecafParser.DeclContext ctx) {
		for(int i = 0; i < ctx.ID().size(); i++){
			defineVar(ctx.tipo(), ctx.ID().get(i).getSymbol());
			//defineVar(ctx.tipo(), ctx.ID().getSymbol());
		}
    }

    @Override
    public void exitDecl(DecafParser.DeclContext ctx) {
		for(int i = 0; i < ctx.ID().size(); i++){
        	String name = ctx.ID().get(i).getSymbol().getText();
			//String name = ctx.ID().getSymbol().getText();
        	Symbol var = currentScope.resolve(name);
        	if ( var==null ) {
            	this.error(ctx.ID().get(i).getSymbol(), "no such variable: "+name);
        	}
        	if ( var instanceof FunctionSymbol) {
            	this.error(ctx.ID().get(i).getSymbol(), name+" is not a variable");
        	}
		}
    }
	
	@Override 
    public void enterVariavel(DecafParser.VariavelContext ctx) {
        for(int i = 0; i < ctx.NUMBER().size(); i++){
            // if(ctx.NUMBER(i).getText().trim().equals("0")){
            //     this.error(ctx.NUMBER(i).getSymbol(),"O ARRAY deve ser maior que 0");
            // }
            if(Integer.parseInt(ctx.NUMBER(i).getText()) <= 0){
                this.error(ctx.NUMBER(i).getSymbol(),"O ARRAY deve ser maior que 0");
            }
        }
     }

	@Override public void exitVariavel(DecafParser.VariavelContext ctx) { }

   	@Override public void enterVar_decl(DecafParser.Var_declContext ctx) { }

    @Override public void exitVar_decl(DecafParser.Var_declContext ctx) { }

	@Override public void enterConteudo(DecafParser.ConteudoContext ctx) { 
        // System.out.println("Teste: " + ctx.getText());
        if(ctx.FOR() != null){
            Token name = ctx.ID().getSymbol();
            
            if(currentScope.resolve(name.getText()) == null){
                defineVar(name);
            }

            try{
                if(ctx.expressao(0).literal().NUMBER() == null){
                    this.error(ctx.FOR().getSymbol(), "Variável do FOR deve ser iniciada com inteiro");
                }
            } catch(Exception ex){}
        }

        if(ctx.IF() != null){
            for(int i = 0; i < ctx.expressao().size(); i++){
                if(ctx.expressao(i).operador() == null){
                    this.error(ctx.IF().getSymbol(), "IF deve ter uma comparação"); 
                }
            }

        }

        try{
            if(ctx.expressao(0).literal() == null){
                this.error(ctx.local().expressao().literal().NUMBER().getSymbol(), "Atribuição deve ser com inteiro");
            }
        } catch(Exception ex){}

        try{
            if(ctx.expressao(0).operador().COMPARACAO() != null){
                if(ctx.expressao(0).expressao(0).literal().NUMBER() == null){
                    this.error(ctx.expressao(0).operador().COMPARACAO().getSymbol(), "Comparação deve ser entre inteiros");
                }
            }
        } catch(Exception ex){}

        String a="boolean";
        String b="boolean";
        try{
            if(ctx.expressao(0).operador().COMPARACAO() != null){
                for(int i = 0; i < ctx.expressao().size(); i++){
                    for(int j = 0; j < ctx.expressao(i).expressao().size(); j++){
                        if(ctx.expressao(i).expressao(j).literal().NUMBER() == null){
                            b = "boolean";
                        }
                        if(ctx.expressao(i).expressao(j).literal().BL() == null){
                            a = "int";
                        }  
                    }  
                }
                if (a != b){
                    this.error(ctx.expressao(0).operador().COMPARACAO().getSymbol(), "Operandos devem ser do mesmo tipo");
                }    
            }        
        }catch(Exception ex){}

        try{
            if(ctx.expressao(0).EXCL() != null){
                if(ctx.expressao(0).expressao(0).literal().NUMBER() != null){
                    this.error(ctx.expressao(0).expressao(0).literal().NUMBER().getSymbol(), "Negação deve ser com boolean");
                }
            
            }
        } catch(Exception ex) {}

        try{
            if(ctx.op_attr().INCREMENTO() != null){
                if(ctx.expressao(0).literal().NUMBER() == null){
                    this.error(ctx.op_attr().INCREMENTO().getSymbol(), "Incremento deve ser com inteiro");
                }
            
            }
        } catch(Exception ex) {}
    }    
	
	@Override public void exitConteudo(DecafParser.ConteudoContext ctx) { 
       
    }

    @Override public void enterLocal(DecafParser.LocalContext ctx) {}

	@Override
    public void exitLocal(DecafParser.LocalContext ctx) { 
		String name = ctx.ID().getSymbol().getText();
        Symbol var = currentScope.resolve(name);
        Symbol varGlobal = globals.resolve(name);
        if ( var==null && varGlobal == null) {
            this.error(ctx.ID().getSymbol(), "Variável não declarada: " + name);
        }
    }

	@Override public void enterParametro(DecafParser.ParametroContext ctx) { }

	@Override 
	public void exitParametro(DecafParser.ParametroContext ctx) {
		for(int i = 0; i < ctx.decl().size(); i++){
			var_metodo = i + 1;
		}
	}

	@Override public void enterChamada_metodo(DecafParser.Chamada_metodoContext ctx) { }

	@Override 
	public void exitChamada_metodo(DecafParser.Chamada_metodoContext ctx) { 
		for(int i = 0; i < ctx.expressao().size(); i++){
			var_chamada_metodo = i + 1;
		}
		if ( var_metodo != var_chamada_metodo) {
            	this.error(ctx.expressao(0).literal().NUMBER().getSymbol(), "Chamada de método com quantidades incorretas de argumentos");
		}
	}

    void defineVar(DecafParser.TipoContext typeCtx, Token nameToken) {
		// System.out.println("Tipo: " + typeCtx.start.getType() + " Token: " + nameToken.getText());
        int typeTokenType = typeCtx.start.getType();
        VariableSymbol var = new VariableSymbol(nameToken.getText());

        // DecafSymbol.Type type = this.getType(typeTokenType);
        // var.setType(type);

        currentScope.define(var); // Define symbol in current scope
    }

    void defineVar(Token nameToken) {
        VariableSymbol var = new VariableSymbol(nameToken.getText());

        currentScope.define(var); // Define symbol in current scope
    }

    /**
    * Método que atuliza o escopo para o atual e imprime o valor
    *
    * @param s
    */
    private void pushScope(Scope s) {
        currentScope = s;
        System.out.println("entering: " + currentScope.getName() + ":" + s);
    }

    /**
    * Método que cria um novo escopo no contexto fornecido
    *
    * @param ctx
    * @param s
    */
    void saveScope(ParserRuleContext ctx, Scope s) {
        scopes.put(ctx, s);
    }

    /**
    * Muda para o contexto superior e atualia o escopo
    */
    private void popScope() {
        System.out.println("leaving: "+currentScope.getName() + ":" + currentScope);
        currentScope = currentScope.getEnclosingScope();
    }

    public static void error(Token t, String msg) {
    	System.err.printf("line %d:%d %s\n", t.getLine(), t.getCharPositionInLine(), msg);
    }

    /**
    * Valida tipos encontrados na linguagem para tipos reais
    *
    * @param tokenType
    * @return
    */
    public static DecafSymbol.Type getType(int tokenType) {
        switch ( tokenType ) {
    		case DecafParser.VOID : return DecafSymbol.Type.tVOID;
        	case DecafParser.NUMBER : return DecafSymbol.Type.tINT;
        }
     	return DecafSymbol.Type.tINVALID;
    }
}
