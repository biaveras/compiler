parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}

/*program: TK_class ID LCURLY RCURLY EOF;*/

program: CLASS PROGRAM LCURLY variavel* metodo* RCURLY EOF;

metodo: (tipo | VOID) ID LPAR parametro* RPAR bloco;

//var_decl: decl (V ID)* PV;
var_decl: decl PV;

parametro: decl (V decl)*;
//decl: tipo ID;
decl: tipo ID (V ID)*;

//variavel: decl (V ID | LCOL NUMBER RCOL)* PV;
variavel: decl (LCOL NUMBER RCOL)* PV;

tipo: INT | BOOLEAN;

bloco: LCURLY var_decl* conteudo* RCURLY;

conteudo: local op_attr expressao PV 
	| chamada_metodo PV
	| CONTINUE PV
	| BREAK PV
	| RETURN expressao? PV
	| FOR ID IGUAL expressao V expressao bloco
	| IF LPAR expressao RPAR bloco (ELSE bloco)? 
	| bloco;

expressao: local 
	| chamada_metodo 
	| literal
	| expressao operador expressao
	| MENOS expressao
	| EXCL expressao
	| LPAR expressao RPAR;

operador: ARITMETICO | COMPARACAO | LOGICO | MENOS;

chamada_metodo: ID LPAR (expressao (V expressao)*)? RPAR
	| CALLOUT LPAR STRING (V callout_arg(V callout_arg)*)? RPAR;

literal: NUMBER | CHAR | BL;

local: ID | ID LCOL expressao RCOL;

op_attr: IGUAL | INCREMENTO | DECREMENTO;

callout_arg: expressao | STRING;
