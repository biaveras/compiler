lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

tokens
{
  TK_class
}

LCURLY : '{';
RCURLY : '}';

LCOL : '[';
RCOL : ']';

LPAR : '(';
RPAR : ')';

BOOLEAN : 'boolean';
BREAK : 'break';
CALLOUT : 'callout';
CLASS : 'class';
CONTINUE : 'continue';
IF :  'if';
ELSE : 'else';
FOR : 'for';
INT : 'int';
RETURN : 'return';
VOID : 'void';
PROGRAM : 'Program';

BL : 'false' | 'true';

V : ',';

PV:  ';';

MENOS : '-';

ARITMETICO : '+' | '*' | '%' | '/';

EXCL : '!';

COMPARACAO : '<' | '<=' | '!=' | '>' | '>=' | '==';

LOGICO : '&&' | '||'; 

IGUAL : '=';

INCREMENTO : '+=';

DECREMENTO : '-=';

NUMBER : HEX | [0-9]+;

ID  : LETRA (LETRA | [0-9]+)*;

WS_ : (' ' | '\n' | '\t') -> skip;

SL_COMMENT : '//' (ASC | '\'')* '\n' -> skip;

CHAR : '\'' ASC '\'';
STRING : '"' ASC* '"';

fragment
ESC :  '\\' ('n' | '"' | 't' | '\\' | '\'');

fragment
ASC : ( ESC | LETRA | NUMBER | CARACTER | V | PV | ARITMETICO | COMPARACAO | LOGICO | IGUAL | LCURLY | RCURLY | LCOL | RCOL | LPAR | RPAR | MENOS | STRING);

fragment
CARACTER : (' ' | EXCL | '#' | '$' | '&' | '.' | ':' | '?' | '@' | '^' | '`' | '~');

fragment
LETRA : [a-zA-Z_];

fragment
HEX : '0x' [0-9a-fA-F]+;
